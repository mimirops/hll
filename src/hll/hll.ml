module List = ListLabels

module P = Process

module String_set = Set.Make(String)
module String_map = CCMap.Make(String)

module Cmdline = struct
  module C = Cmdliner

  let generate_cmd generate_pkg =
    let pkg_name =
      let doc = "Name of the package, defaults to current dir name" in
      C.Arg.(value & opt (some string) None & info ["n"; "name"] ~docv:"NAME" ~doc)
    in
    let opam_dir =
      let doc = "Path to the output opam repo directory" in
      C.Arg.(required & opt (some dir) None & info ["opam-dir"] ~docv:"DIR" ~doc)
    in
    let tag =
      let doc = "Tag to use for the version of the package" in
      C.Arg.(value & opt (some string) None & info ["tag"] ~docv:"TAG" ~doc)
    in
    let pins_file =
      let doc = "Path to file containing pins (default hll.pins)" in
      C.Arg.(value & opt (some file) None & info ["pins"] ~docv:"FILE" ~doc)
    in
    let hll_conf =
      let doc = "Path to hll config for this repo, hll.conf by default" in
      C.Arg.(value & opt file "hll.conf" & info ["hll"] ~docv:"FILE" ~doc)
    in
    let pds_conf =
      let doc = "Path to pds config for this repo, pds.conf by default" in
      C.Arg.(value & opt file "pds.conf" & info ["pds"] ~docv:"FILE" ~doc)
    in
    let test_deps_as_regular_deps =
      let doc = "Test deps will not get a 'test' constraint, workaround for opam install -t bug" in
      C.Arg.(value & flag & info ["test-deps-as-regular-deps"] ~doc)
    in
    let doc = "generate an opam package from a pds and an hll config" in
    C.Term.(const generate_pkg $
            pkg_name $
            opam_dir $
            tag $
            pins_file $
            hll_conf $
            pds_conf $
            test_deps_as_regular_deps,
            info "generate" ~doc)

  let default_cmd =
    let doc = "Generate opam packages from pds builds" in
    C.Term.(ret (const (`Help (`Pager, None))),
            info "help" ~doc)
end

let default_selector_map =
  let make_pair x = (x, x) in
  String_map.of_list
    [ make_pair "darwin"
    ; make_pair "linux"
    ; make_pair "freebsd"
    ; make_pair "openbsd"
    ; make_pair "netbsd"
    ; make_pair "dragonfly"
    ; make_pair "cygwin"
    ; make_pair "win32"
    ; make_pair "unix"
    ]

let value_opt ~default = function
  | Some v -> v
  | None -> default

let value_exn ~msg = function
  | Some v -> v
  | None -> failwith msg

module Generate = struct
  type url_proto =
    | Git
    | Http

  module Pds_version = struct
    type t =
      | Major of int
      | Version of string
  end

  type t = { tag : string
           ; url_protocol : url_proto
           ; url_template : string
           ; pattern : string
           ; desc : string
           ; maintainer : string
           ; external_deps : String_set.t
           ; test_deps : String_set.t
           ; deps_blacklist : String_set.t
           ; deps_map : TomlTypes.table
           ; pins : string String_map.t
           ; homepage : string
           ; authors : String_set.t
           ; bug_reports : string
           ; dev_repo : string
           ; available : string
           ; build_deps : String_set.t
           ; opam_extra_lines : string list
           ; pds_version : Pds_version.t
           }

  let url_section t =
    let url =
      CCString.replace ~which:`Left ~sub:t.pattern ~by:t.tag t.url_template
    in
    match t.url_protocol with
      | Git ->
        Printf.sprintf "\tsrc: \"%s\"\n" url
      | Http ->
        let lines = P.read_stdout "curl" [| "-L"; url |] in
        (*
         * Process is, unfortunately, somewhat lame in that it breaks lines up
         * from the output rather than giving the raw output.  So join them again
         * and add the new line back.
         *)
        let data = String.concat "\n" lines in
        let hex = Digest.string data |> Digest.to_hex in
        Printf.sprintf "\tsrc: \"%s\"\n\tchecksum: \"%s\"" url hex

  let format_external_deps deps pins =
    List.map
      ~f:(fun d ->
        match String_map.get d pins with
          | Some pin ->
            Printf.sprintf "\"%s\" { %s }" d pin
          | None ->
            Printf.sprintf "\"%s\"" d)
      (String_set.elements deps)

  let format_authors = List.map ~f:(Printf.sprintf "\"%s\"")

  let str_if_not_empty k = function
    | "" -> []
    | v -> [Printf.sprintf "%s: \"%s\"" k v]

  let list_if_not_empty k = function
    | "" -> []
    | v -> [Printf.sprintf "%s: [%s]" k v]

  let concat_nl = String.concat "\n"
  let concat_nltab = String.concat "\n\t"

  let maybe_add_pds_pin pds_version pins =
    match String_map.get "pds" pins with
      | Some _ ->
        pins
      | None ->
        let version =
          match pds_version with
          | Pds_version.Major mv ->
            Printf.sprintf "(>= \"%d\" & < \"%d\")" mv (mv + 1)
          | Pds_version.Version v ->
            v
        in
        String_map.add "pds" (Printf.sprintf "build & %s" version) pins

  let maybe_add_test_pins test_deps pins =
    String_set.fold
      (fun d pins ->
         match String_map.get d pins with
           | Some _ ->
             pins
           | None ->
             String_map.add d "with-test" pins)
      test_deps
      pins

  let generate_pkg_content t =
    let pins =
      t.pins
      |> maybe_add_pds_pin t.pds_version
      |> maybe_add_test_pins t.test_deps
    in
    let all_build_deps =
      String_set.union
        t.build_deps
        (String_set.of_list ["ocamlfind"; "pds"])
    in
    (* The set of compile-time deps that map to package names *)
    let deps_map_src =
      String_set.of_list
        (CCList.flat_map
           (fun (_, v) ->
              value_exn
                ~msg:"Invalid deps_map value"
                TomlLenses.(get v (array |-- strings)))
           (TomlTypes.Table.bindings t.deps_map))
    in
    (* The set of packages that the compile-time deps map to *)
    let deps_map_dst =
      String_set.of_list
        (List.map
           ~f:(fun (k, _) -> TomlTypes.Table.Key.to_string k)
           (TomlTypes.Table.bindings t.deps_map))
    in
    let blacklist = String_set.union t.deps_blacklist deps_map_src in
    let build_deps =
      deps_map_dst
      |> String_set.union t.external_deps
      |> String_set.union all_build_deps
      |> String_set.union t.test_deps
    in
    let output_deps = String_set.diff build_deps blacklist in
    let external_deps = format_external_deps output_deps pins in
    concat_nl
      (List.concat
         [ [ "opam-version: \"2.0\""
           ; Printf.sprintf "maintainer: \"%s\"" t.maintainer
           ; "build: ["
           ; "\t[make \"-j%{jobs}%\"]"
           ; "\t[make \"-j%{jobs}%\" \"test\"] {with-test}"
           ; "]"
           ; ""
           ; "install: ["
           ; "\t[make \"PREFIX=%{prefix}%\" \"install\"]"
           ; "]"
           ; ""
           ; "remove: ["
           ; "\t[make \"PREFIX=%{prefix}%\" \"remove\"]"
           ; "]"
           ; ""
           ; "depends: ["
           ; "\t" ^ concat_nltab external_deps
           ; "]"
           ; ""
           ; "authors: ["
           ; "\t" ^ concat_nltab (format_authors (String_set.elements t.authors))
           ; "]"
           ; ""
           ; "description: \"\"\""
           ; t.desc
           ; "\"\"\""
           ; ""
           ; Printf.sprintf "homepage: \"%s\"" t.homepage
           ; ""
           ; "url {"
           ; url_section t
           ; "}"
           ]
         ; str_if_not_empty "bug-reports" t.bug_reports
         ; str_if_not_empty "dev-repo" t.dev_repo
         ; list_if_not_empty "available" t.available
         ; t.opam_extra_lines
         ; [""]
         ])
end

module Generate_io = struct
  module Hll_conf = struct
    type t = { maintainer : string
             ; url_template : string
             ; url_protocol : Generate.url_proto
             ; url_pattern : string
             ; desc : string
             ; deps_blacklist : String_set.t
             ; deps_map : TomlTypes.table
             ; homepage : string
             ; authors : String_set.t
             ; bug_reports : string
             ; dev_repo : string
             ; build_deps : String_set.t
             ; available : string
             ; opam_extra_lines : string list
             ; pds_version : Generate.Pds_version.t
             ; selector_map : string String_map.t
             }

    let url_protocol_of_string = function
      | "git" -> Generate.Git
      | "http" -> Generate.Http
      | s -> failwith (Printf.sprintf "Invalid url_protocol (%s) in hll configuration" s)

    let selector_map table =
      TomlTypes.Table.(
        fold
          (fun key v selectors ->
             let filter =
               value_exn
                 ~msg:"Selector map must be string to string"
                 TomlLenses.(get v string)
             in
             String_map.add (Key.to_string key) filter selectors)
          table
          String_map.empty)

    let get_selector_map hll_conf =
      let sm = TomlLenses.(get hll_conf (key "selector_map" |-- table)) in
      match sm with
        | Some table ->
          selector_map table
        | None ->
          default_selector_map

    let read fname =
      let hll_conf = Toml.Parser.(from_filename fname |> unsafe) in
      let maintainer =
        value_exn
          ~msg:"A maintainer is required"
          TomlLenses.(get hll_conf (key "maintainer" |-- string))
      in
      let url_template =
        value_exn
          ~msg:"A url_template is required"
          TomlLenses.(get hll_conf (key "url_template" |-- string))
      in
      let url_protocol =
        value_exn
          ~msg:"A url_protocol is required"
          TomlLenses.(get hll_conf (key "url_protocol" |-- string))
      in
      let url_pattern =
        value_exn
          ~msg:"A url_pattern is required"
          TomlLenses.(get hll_conf (key "url_pattern" |-- string))
      in
      let desc =
        value_exn
          ~msg:"A desc is required"
          TomlLenses.(get hll_conf (key "desc" |-- string))
      in
      let authors =
        value_exn
          ~msg:"A list of authors is required"
          TomlLenses.(get hll_conf (key "authors" |-- array |-- strings))
      in
      let homepage =
        value_exn
          ~msg:"A homepage is required"
          TomlLenses.(get hll_conf (key "homepage" |-- string))
      in
      let bug_reports =
        value_opt
          ~default:""
          TomlLenses.(get hll_conf (key "bug_reports" |-- string))
      in
      let dev_repo =
        value_opt
          ~default:""
          TomlLenses.(get hll_conf (key "dev_repo" |-- string))
      in
      let deps_blacklist =
        value_opt
          ~default:[]
          TomlLenses.(get hll_conf (key "deps_blacklist" |-- array |-- strings))
      in
      let deps_map =
        value_opt
          ~default:TomlTypes.Table.empty
          TomlLenses.(get hll_conf (key "deps_map" |-- table))
      in
      let build_deps =
        value_opt
          ~default:[]
          TomlLenses.(get hll_conf (key "build_deps" |-- array |-- strings))
      in
      let available =
        value_opt
          ~default:""
          TomlLenses.(get hll_conf (key "available" |-- string))
      in
      let opam_extra_lines =
        value_opt
          ~default:[]
          TomlLenses.(get hll_conf (key "opam_extra_lines" |-- array |-- strings))
      in
      let pds_major_version =
        TomlLenses.(get hll_conf (key "pds" |-- table |-- key "major_version" |-- int))
      in
      let pds_version_str =
        TomlLenses.(get hll_conf (key "pds" |-- table |-- key "version" |-- string))
      in
      let pds_version =
        match (pds_major_version, pds_version_str) with
          | (None, None) ->
            failwith "Must specify pds.major_version or pds.version"
          | (Some _, Some _) ->
            failwith "Can only specify one of pds.major_version or pds.version"
          | (Some mv, _) ->
            Generate.Pds_version.Major mv
          | (_, Some v) ->
            Generate.Pds_version.Version v
      in
      let selector_map = get_selector_map hll_conf in
      { maintainer = maintainer
      ; url_template = url_template
      ; url_protocol = url_protocol_of_string url_protocol
      ; url_pattern = url_pattern
      ; desc = desc
      ; authors = String_set.of_list authors
      ; homepage = homepage
      ; bug_reports = bug_reports
      ; deps_blacklist = String_set.of_list deps_blacklist
      ; deps_map = deps_map
      ; dev_repo = dev_repo
      ; available = available
      ; build_deps = String_set.of_list build_deps
      ; opam_extra_lines = opam_extra_lines
      ; pds_version = pds_version
      ; selector_map = selector_map
      }
  end

  module Pds_conf = struct
    type t = { deps : string list String_map.t (* Dep -> Selectors *)
             ; tests_deps : string list String_map.t (* Dep -> Selectors *)
             ; targets : String_set.t
             ; selectors : String_set.t
             ; used_selectors : String_set.t
             }

    let default_selector = ""

    let list_keys table =
      TomlTypes.Table.(
        fold
          (fun key _ keys -> Key.to_string key::keys)
          table
          [])

    let list_deps table =
      TomlTypes.Table.(
        fold
          (fun key v deps ->
             deps @ value_opt
               ~default:[]
               TomlLenses.(get v (table |-- key "deps" |-- array |-- strings)))
          table
          [])

    (* Merge entries in a table and append lists. *)
    let merge k v1 v2 =
      match (v1, v2) with
        | (Some v1, Some v2) -> Some (v1 @ v2)
        | (Some v, None)
        | (None, Some v) -> Some v
        | (None, None) -> assert false

    (* Return a tuple of to String_map's.  The first is the map for builds and
       the second is the deps for tests. *)
    let selector_deps pds_conf =
      (* Fold but specifically pick out the selectors of the entries in the
         table. *)
      let fold_selectors tbl f init =
        TomlTypes.Table.fold
          (fun build v acc ->
             let selector =
               value_opt
                 ~default:TomlTypes.Table.empty
                 TomlLenses.(get v (table |-- key "selector" |-- table))
             in
             f selector acc)
          tbl
          init
      in
      (* Look over all of the selectors in a table and merge their deps ->
         selector map with the passed in map. *)
      let merge_deps selector acc =
        TomlTypes.Table.fold
          (fun slct v acc ->
             let slct = TomlTypes.Table.Key.to_string slct in
             let deps =
               value_opt
                 ~default:[]
                 TomlLenses.(get v (table |-- key "deps" |-- array |-- strings))
             in
             deps
             |> ListLabels.map ~f:(fun d -> (d, [slct]))
             |> String_map.of_list
             |> String_map.merge merge acc)
          selector
          acc
      in
      let src =
        value_opt
          ~default:TomlTypes.Table.empty
          TomlLenses.(get pds_conf (key "src" |-- table))
      in
      let tests =
        value_opt
          ~default:TomlTypes.Table.empty
          TomlLenses.(get pds_conf (key "tests" |-- table))
      in
      let src_deps =
        fold_selectors
          src
          merge_deps
          String_map.empty
      in
      let tests_deps =
        fold_selectors
          tests
          merge_deps
          String_map.empty
      in
      (src_deps, tests_deps)

    let read test_deps_as_regular_deps fname sm =
      let pds_conf = Toml.Parser.(from_filename fname |> unsafe) in
      let srcs =
        value_opt
          ~default:TomlTypes.Table.empty
          TomlLenses.(get pds_conf (key "src" |-- table))
      in
      let tests =
        value_opt
          ~default:TomlTypes.Table.empty
          TomlLenses.(get pds_conf (key "tests" |-- table))
      in
      let src_default_deps =
        String_map.of_list
          (ListLabels.map ~f:(fun d -> (d, [default_selector])) (list_deps srcs))
      in
      let tests_default_deps =
        String_map.of_list
          (ListLabels.map ~f:(fun d -> (d, [default_selector])) (list_deps tests))
      in
      let targets = String_set.of_list (list_keys srcs) in
      let (deps, tests_deps) = selector_deps pds_conf in
      (* Convert the selectors via the map *)
      (* TODO Make this get less exceptiony *)
      let deps =
        String_map.merge
          merge
          (String_map.map (ListLabels.map ~f:(CCFun.flip String_map.find sm)) deps)
          src_default_deps
      in
      let tests_deps =
        String_map.merge
          merge
          (String_map.map (ListLabels.map ~f:(CCFun.flip String_map.find sm)) tests_deps)
          tests_default_deps
      in
      (* Those deps that are just in the test section *)
      let tests_only_deps = String_map.filter (fun k _ -> not (String_map.mem k deps)) tests_deps in
      let selectors = String_set.of_list (ListLabels.map ~f:snd (String_map.bindings sm)) in
      let used_selectors =
        String_set.remove
          default_selector
          (String_set.of_list
             (List.concat
                (ListLabels.map ~f:snd (String_map.bindings deps) @
                 ListLabels.map ~f:snd (String_map.bindings tests_only_deps))))
      in
      if not test_deps_as_regular_deps then
        { deps = deps
        ; tests_deps = tests_only_deps
        ; targets = targets
        ; selectors = selectors
        ; used_selectors = used_selectors
        }
      else
        { deps = String_map.merge merge deps tests_only_deps
        ; tests_deps = String_map.empty
        ; targets = targets
        ; selectors = selectors
        ; used_selectors = used_selectors
        }

    let external_deps t =
      String_set.diff
        (String_set.of_list (ListLabels.map ~f:fst (String_map.bindings t.deps)))
        t.targets

    let test_external_deps t =
      String_set.diff
        (String_set.of_list (ListLabels.map ~f:fst (String_map.bindings t.tests_deps)))
        t.targets

    let only_in_default_selector v _ = v = [default_selector]

    let has_default_selector v _ = ListLabels.mem default_selector ~set:v

    let remove_default_selector v _ = ListLabels.filter ~f:((<>) default_selector) v
  end

  module Pins = struct
    type t = (string * string) list

    let empty = String_map.empty

    let read fname =
      let lines = CCIO.with_in fname CCIO.read_lines_l in
      let pins =
        List.fold_left
          ~f:(fun acc line ->
            match CCString.Split.left ~by:" " line with
              | Some (dep, version) ->
                String_map.add (String.trim dep) (String.trim version) acc
              | None ->
                failwith "Invalid pinning, format is \"dep version\"")
          ~init:String_map.empty
          lines
      in
      pins
  end

  let rec mkdir_p path =
    match Filename.dirname path with
      | _ when Sys.file_exists path ->
        ()
      | "." ->
        Unix.mkdir path 0o755
      | p ->
        mkdir_p p;
        Unix.mkdir path 0o755

  let output_pkg ~opam_dir ~name ~tag ~contents =
    let pkg_dir = Filename.concat opam_dir "packages" in
    let base_pkg_dir = Filename.concat pkg_dir name in
    let base_dir = Filename.concat base_pkg_dir (Printf.sprintf "%s.%s" name tag) in
    mkdir_p base_dir;
    CCIO.with_out (Filename.concat base_dir "opam") (CCFun.flip CCIO.write_line contents)
end

let error_if_failed = function
  | P.Exit.Exit 0 -> ()
  | _ -> failwith "Unable to determine tag"

let maybe_load_tag = function
  | Some tag ->
    tag
  | None -> begin
    let output = P.run "git" [| "describe" |] in
    error_if_failed output.P.Output.exit_status;
    String.concat "" output.P.Output.stdout
  end

let maybe_load_pins = function
  | Some file -> Generate_io.Pins.read file
  | None when Sys.file_exists "hll.pins" -> Generate_io.Pins.read "hll.pins"
  | None -> Generate_io.Pins.empty

(* TODO Clean up all the duplication *)
let merge_pins_with_selector_deps pins pds_conf =
  let pins =
    String_map.fold
      (fun dep v acc ->
         (* If there is already a pin, it overrides anything we want to do. *)
         match String_map.get dep acc with
           | Some _ ->
             acc
           | None when Generate_io.Pds_conf.only_in_default_selector v pds_conf ->
             acc
           | None when Generate_io.Pds_conf.has_default_selector v pds_conf ->
             let used_selectors = pds_conf.Generate_io.Pds_conf.used_selectors in
             let v = String_set.of_list (Generate_io.Pds_conf.remove_default_selector v pds_conf) in
             let negative_selectors = String_set.diff used_selectors v in
             if not (String_set.is_empty negative_selectors) then
               let pin =
                 "!(" ^ (String.concat " | " (String_set.elements negative_selectors)) ^ ")"
               in
               String_map.add dep pin acc
             else
               acc
           | None ->
             let pin = String.concat " | " (List.map (Printf.sprintf "os = \"%s\"") v) in
             String_map.add dep pin acc)
      pds_conf.Generate_io.Pds_conf.deps
      pins
  in
  String_map.fold
    (fun dep v acc ->
       (* If there is already a pin, it overrides anything we want to do. *)
       match String_map.get dep acc with
         | Some _ ->
           acc
         | None when Generate_io.Pds_conf.only_in_default_selector v pds_conf ->
           acc
         | None when Generate_io.Pds_conf.has_default_selector v pds_conf ->
           let selectors = pds_conf.Generate_io.Pds_conf.selectors in
           let v = String_set.of_list (Generate_io.Pds_conf.remove_default_selector v pds_conf) in
           let negative_selectors = String_set.diff selectors v in
           let pin =
             if not (String_set.is_empty negative_selectors) then
               "with-test & !(" ^ (String.concat " | " (String_set.elements negative_selectors)) ^ ")"
             else
               "with-test"
           in
           String_map.add dep pin acc
         | None ->
           let pin = "with-test & (" ^ (String.concat " | " v) ^ ")" in
           String_map.add dep pin acc)
    pds_conf.Generate_io.Pds_conf.tests_deps
    pins

let generate_pkg
    name_opt
    opam_dir
    (tag_opt : string option)
    pins_file_opt
    hll_conf
    pds_conf
    test_deps_as_regular_deps =
  let tag = maybe_load_tag tag_opt in
  let hll_conf = Generate_io.Hll_conf.read hll_conf in
  let selector_map = hll_conf.Generate_io.Hll_conf.selector_map in
  let pds_conf = Generate_io.Pds_conf.read test_deps_as_regular_deps pds_conf selector_map in
  let pins = merge_pins_with_selector_deps (maybe_load_pins pins_file_opt) pds_conf in
  let gen_t =
    Generate.({ tag = tag
              ; url_protocol = hll_conf.Generate_io.Hll_conf.url_protocol
              ; url_template = hll_conf.Generate_io.Hll_conf.url_template
              ; pattern = hll_conf.Generate_io.Hll_conf.url_pattern
              ; desc = hll_conf.Generate_io.Hll_conf.desc
              ; maintainer = hll_conf.Generate_io.Hll_conf.maintainer
              ; external_deps = Generate_io.Pds_conf.external_deps pds_conf
              ; test_deps = Generate_io.Pds_conf.test_external_deps pds_conf
              ; deps_blacklist = hll_conf.Generate_io.Hll_conf.deps_blacklist
              ; deps_map = hll_conf.Generate_io.Hll_conf.deps_map
              ; pins = pins
              ; homepage = hll_conf.Generate_io.Hll_conf.homepage
              ; authors = hll_conf.Generate_io.Hll_conf.authors
              ; bug_reports = hll_conf.Generate_io.Hll_conf.bug_reports
              ; dev_repo = hll_conf.Generate_io.Hll_conf.dev_repo
              ; available = hll_conf.Generate_io.Hll_conf.available
              ; build_deps = hll_conf.Generate_io.Hll_conf.build_deps
              ; opam_extra_lines = hll_conf.Generate_io.Hll_conf.opam_extra_lines
              ; pds_version = hll_conf.Generate_io.Hll_conf.pds_version
              })
  in
  let name = CCOpt.get_or ~default:(Filename.basename (Sys.getcwd ())) name_opt in
  let contents = Generate.generate_pkg_content gen_t in
  Generate_io.output_pkg
    ~opam_dir
    ~name
    ~tag
    ~contents

let cmds = [Cmdline.generate_cmd generate_pkg]

let main () =
  match Cmdliner.Term.eval_choice Cmdline.default_cmd cmds with
    | `Error _ -> exit 1
    | _ -> exit 0

let () = main ()
