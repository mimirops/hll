.PHONY: all test smoke-tests
all:
	pds
	$(MAKE) -f pds.mk all

test: smoke-tests

%:
	pds
	$(MAKE) -f pds.mk $*


smoke-tests: release
	cd smoke-tests && ./test
